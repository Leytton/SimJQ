function test1() {
	//$(),$s()都行，与Jquery冲突时在Jquery之前引入js文件，采用$s()
	$('#test1').text('Hello,I\'m Leytton~');
	alert($('#test1').text());
}

function test2() {
	$('#test2').attr('my_name', '不亦');
	alert($('#test2').attr('my_name'));
}

function test3() {
	$('#test3').html('<a href="http://blog.csdn.net/leytton" target="_blank"><img src="img/sa.jpg" />图文超链接</a>');
	alert($('#test3').html());
}

function test4() {
	if ('1' == $('#test4').attr('show')) {
		$('#test4').hide();
		$('#test4').attr('show', '0');
	} else {
		$('#test4').show();
		$('#test4').attr('show', '1');
	}
}

function test5() {
	alert($('#test5').val());
}

function test6() {
	$('#test6').val('Hello,SimJQ~');
}

function test7() {
	$('.test7').eq(0).text('Hi,SimJQ~');
	alert($('.test7').eq(0).text());
}

function test8() {
	$('.test8').eq(1, 2).text('Hi,SimJQ~');
	alert($('.test8').eq(1, 2).text());
}

function test9() {
	$('.test9').text('Hi,SimJQ~');
	var str = '';
	var str_list = $('.test9').text();
	for (var i = 0; i < str_list.length; i++) {
		str += (i + ':' + str_list[i] + '\n');
	}
	alert('length:' + str_list.length + '\n' + str);
}

function test10() {
	$('.test10').eq(0).attr('my_name', '不亦');
	alert($('.test10').eq(0).attr('my_name'));
}

function test11() {
	$('.test11').eq(1, 2).attr('my_name', '不亦');
	alert($('.test11').eq(1, 2).attr('my_name'));
}

function test12() {
	$('.test12').attr('my_name', '不亦');
	var str = '';
	var str_list = $('.test12').attr('my_name');
	for (var i = 0; i < str_list.length; i++) {
		str += (i + ':' + str_list[i] + '\n');
	}
	alert('length:' + str_list.length + '\n' + str);
}

function test13() {
	$('.test13').eq(0).html('<a href="http://blog.csdn.net/leytton" target="_blank"><img src="img/sa.jpg" />图文超链接</a>');
	alert($('.test13').eq(0).html());
}

function test14() {
	$('.test14').eq(1, 2).html('<a href="http://blog.csdn.net/leytton" target="_blank"><img src="img/sa.jpg" />图文超链接</a>', 1);
	alert($('.test14').eq(1, 2).html());
}

function test15() {
	$('.test15').html('<a href="http://blog.csdn.net/leytton" target="_blank"><img src="img/sa.jpg" />图文超链接</a>');
	var str = '';
	var str_list = $('.test15').html();
	for (var i = 0; i < str_list.length; i++) {
		str += (i + ':' + str_list[i] + '\n');
	}
	alert('length:' + str_list.length + '\n' + str);
}

function test16() {
	if ('0' == $('#eg16').attr('show')) {
		$('.test16').eq(0).show();
		$('#eg16').attr('show', '1');
	} else {
		$('.test16').eq(0).hide();
		$('#eg16').attr('show', '0');
	}
}

function test17() {
	if ('0' == $('#eg17').attr('show')) {
		$('.test17').eq(1, 2).show();
		$('#eg17').attr('show', '1');
	} else {
		$('.test17').eq(1, 2).hide();
		$('#eg17').attr('show', '0');
	}
}

function test18() {
	if ('0' == $('#eg18').attr('show')) {
		$('.test18').show();
		$('#eg18').attr('show', '1');
	} else {
		$('.test18').hide();
		$('#eg18').attr('show', '0');
	}
}

function test19() {
	alert('元素对象:' + $('#test19')[0] + '\n文本:' + $('#test19')[0].innerText);
}

function test20() {
	alert('元素对象:' + $('.test20')[0] + '\n文本:' + $('.test20')[0].innerText);
}

function test21() {
	alert('元素对象:' + $('.test21')[1] + '\n文本:' + $('.test21')[1].innerText);
}

function test22() {
	var str = '';
	for (var i = 0; i < $('.test22').length; i++) {
		str += (i + ':' + $('.test22')[i].innerText + '\n');
	}
	alert('元素对象数组:' + $('.test22') + '\n' + str);
}

function test23() {
	$('.test23').eq(0).val('Hi,SimJQ~');
	alert($('.test23').eq(0).val());
}

function test24() {
	$('.test24').eq(1, 2).val('Hi,SimJQ~');
	alert($('.test24').eq(1, 2).val());
}

function test25() {
	$('.test25').val('Hi,SimJQ~');
	var str = '';
	var str_list = $('.test25').val();
	for (var i = 0; i < str_list.length; i++) {
		str += (i + ':' + str_list[i] + '\n');
	}
	alert('length:' + str_list.length + '\n' + str);
}

function test26() {
	$('#test26').click(function () {
		alert(this.innerText);
	});
}

function test27() {
	$('.test27').eq(0).click(function () {
		alert(this.innerText);
	});
}

function test28() {
	$('.test28').eq(1, 2).click(function () {
		alert(this.innerText);
	}, 1);
}

function test29() {
	$('.test29').click(function () {
		alert(this.innerText);
	});
}

function test30() {
	var str = '';
	var i = 0;
	$('.test30').each(function () {
		str += (i + ':' + this.innerText + '\n');
		i++;
	});
	alert('length:' + i + '\n' + str);
}

function test31() {
	$('#test31').remove();
}

function test32() {
	$('.test32').eq(0).remove();
}

function test33() {
	$('.test33').eq(1, 2).remove();
}

function test34() {
	$('.test34').remove();
}

function test35() {
	$('#test35').append('hello');
}

function test36() {
	$('.test36').eq(0).append('<a href="http://www.llqqww.com" target="_blank">www.llqqww.com</a>');
}

function test37() {
	$('.test37').eq(1, 2).append('hello');
}

function test38() {
	$('.test38').append('<a href="http://www.llqqww.com" target="_blank">www.llqqww.com</a>');
}

function test39() {
	$('#test39').prepend('hello');
}

function test40() {
	$('.test40').eq(0).prepend('<a href="http://www.llqqww.com" target="_blank">www.llqqww.com</a>');
}

function test41() {
	$('.test41').eq(1, 2).prepend('hello');
}

function test42() {
	$('.test42').prepend('<a href="http://www.llqqww.com" target="_blank">www.llqqww.com</a>');
}

function test43() {
	$('#test43').addClass('red');
}

function test44() {
	$('.test44').eq(0).addClass('red');
}

function test45() {
	$('.test45').eq(1, 2).addClass('red');
}

function test46() {
	$('.test46').addClass('red');
}

function test47() {
	$('#test47').removeClass('red');
}

function test48() {
	$('.test48').eq(0).removeClass('red');
}

function test49() {
	$('.test49').eq(1, 2).removeClass('red');
}

function test50() {
	$('.test50').removeClass('red');
}

function test98() {
	var url = 'http://www.llqqww.com/open/json/get.php';
	$.ajax({
		method: 'GET',
		dataType: 'json',
		url: url,
		data: {
			name: 'Leytton'
		},
		success: function (data, xhr) {
			alert(JSON.stringify(data));
		},
		error: function (data, xhr) {
			alert(data);
		}
	});
}

function test99() {
	var url = 'http://www.llqqww.com/open/json/post.php';
	$.ajax({
		method: 'POST',
		dataType: 'json',
		url: url,
		data: {
			name: 'Leytton'
		},
		success: function (data, xhr) {
			alert(JSON.stringify(data));
		},
		error: function (data, xhr) {
			alert(data);
		}
	});
}
